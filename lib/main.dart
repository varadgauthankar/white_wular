import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:white_wular/constant/prefs_keys.dart';
import 'package:white_wular/pages/auth_pages/auth_wrapper.dart';

import 'package:white_wular/provider/auth_providers/login_provider.dart';
import 'package:white_wular/provider/auth_providers/register_provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = await SharedPreferences.getInstance();

  runApp(MyApp(
    isUserLoggedIn: prefs.getBool(userPrefsKey) ?? false,
  ));
}

class MyApp extends StatelessWidget {
  final bool isUserLoggedIn;
  const MyApp({required this.isUserLoggedIn, super.key});

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => RegisterProvider()),
        ChangeNotifierProvider(create: (context) => LoginProvider()),
      ],
      child: MaterialApp(
        title: 'White Wular',
        theme: ThemeData(
          useMaterial3: true,
          primarySwatch: Colors.amber,
        ),
        home: AuthWrapper(isUserLoggedIn: isUserLoggedIn),
      ),
    );
  }
}
