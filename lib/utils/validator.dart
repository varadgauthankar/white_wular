class Validators {
  static String? isValidEmail(String? input) {
    if (RegExp(r'^\S+@\S+$').hasMatch(input!)) {
      return null;
    } else {
      return 'Please enter a valid email';
    }
  }

  static String? isValidPassword(String? input) {
    if (input!.length < 6) {
      return 'Password must be at least 6 characters long';
    }

    return null;
  }

  static String? isValidText(String? input) {
    if (input!.isEmpty) {
      return 'Please enter a valid text';
    }

    if (input.length < 3) {
      return 'Name too short';
    }

    return null;
  }

  static String? isValidPhoneNumber(String? input) {
    if (input!.isEmpty) {
      return 'Please enter a valid phone number';
    }

    if (input.length < 10) {
      return 'Enter a valid phone number';
    }

    return null;
  }
}
