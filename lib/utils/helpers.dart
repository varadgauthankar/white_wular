import 'package:flutter/material.dart';

Widget spacer({double height = 0, double width = 0}) {
  return SizedBox(height: height, width: width);
}

void goToPage(BuildContext context, Widget page) {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => page),
  );
}

void replaceAllPages(BuildContext context, Widget page) {
  Navigator.pushAndRemoveUntil(
    context,
    MaterialPageRoute(builder: (context) => page),
    (route) => false,
  );
}
