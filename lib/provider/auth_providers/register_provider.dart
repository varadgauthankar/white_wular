import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:white_wular/constant/dio.dart';
import 'package:white_wular/constant/prefs_keys.dart';

import '../../services/prefs.dart';

class RegisterProvider extends ChangeNotifier {
  String? _firstName;
  String? _lastName;
  String? _email;
  String? _phoneNumber;

  String? _birthDate;
  String _gender = 'Male'; // hard coded for now
  String get gender => _gender;
  String _profession = 'Student'; // hard coded for now
  String get profession => _profession;

  String? _password;

  String? _errorMessage;
  String? get errorMessage => _errorMessage;

  RegisterState _state = RegisterState.initial;
  RegisterState get state => _state;

  // controllers

  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneNumberController = TextEditingController();
  TextEditingController birthDateController = TextEditingController();

  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  // form key

  GlobalKey<FormState> formKey1 = GlobalKey<FormState>();
  GlobalKey<FormState> formKey2 = GlobalKey<FormState>();
  GlobalKey<FormState> formKey3 = GlobalKey<FormState>();

  Future<void> register() async {
    Dio dio = await MyDio.provideDio();
    _errorMessage = null;

    try {
      _setState(RegisterState.loading);
      final result = await dio.post('/CustomerRegister', data: {
        "dob": _birthDate,
        "emailId": emailController.text,
        "firstName": firstNameController.text,
        "gender": _gender,
        "hobbies": "",
        "lastName": lastNameController.text,
        "password": passwordController.text,
        "phoneNumber": passwordController.text,
        "profession": _profession,
        "socialprofile": "",
        "type": ""
      });
      if (result.statusCode == 200 && result.data['successful'] == true) {
        _setState(RegisterState.complete);

        // usually its a good idea to save user object
        // but api is not returning user object
        // so just saving a boolean value for now
        await PrefsService.setBool(userPrefsKey, true);
      } else {
        _errorMessage = result.data['msg'];
        _setState(RegisterState.error);
      }
    } on DioError catch (e) {
      _errorMessage = e.message;
      _setState(RegisterState.error);
    }
  }

  Future<String> pickDate(BuildContext context) async {
    final pickedDate = await showDatePicker(
        context: context,
        initialDate:
            _birthDate != null ? DateTime.parse(_birthDate!) : DateTime.now(),
        firstDate: DateTime(1900),
        lastDate: DateTime.now());

    _birthDate = pickedDate.toString();
    birthDateController.text = DateFormat('dd/MM/yyyy').format(pickedDate!);

    notifyListeners();

    return _birthDate!;
  }

  setGender(String gender) {
    _gender = gender;
    notifyListeners();
  }

  setProfession(String profession) {
    _profession = profession;
    notifyListeners();
  }

  bool validateForm1() {
    final value = formKey1.currentState?.validate();
    if (value!) {
      return true;
    }
    return false;
  }

  bool validateForm2() {
    final value = formKey2.currentState?.validate();
    if (value!) {
      return true;
    }
    return false;
  }

  bool validateForm3() {
    final value = formKey3.currentState?.validate();
    if (value!) {
      return true;
    }
    return false;
  }

  void _setState(RegisterState state) {
    _state = state;
    notifyListeners();
  }
}

enum RegisterState {
  initial,
  loading,
  complete,
  error,
}
