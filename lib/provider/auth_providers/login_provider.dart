import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:white_wular/constant/prefs_keys.dart';
import 'package:white_wular/services/prefs.dart';

import '../../constant/dio.dart';

class LoginProvider extends ChangeNotifier {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  String? _errorMessage;
  String? get errorMessage => _errorMessage;

  LoginState _state = LoginState.initial;
  LoginState get state => _state;

  Future<void> login() async {
    Dio dio = await MyDio.provideDio();
    _errorMessage = null;

    try {
      _setState(LoginState.loading);
      final result = await dio.post('/login', data: {
        "emailIdOrPhone": emailController.text,
        "password": passwordController.text,
        "sessionKey": "string"
      });

      if (result.statusCode == 200 && result.data['successful'] == true) {
        _setState(LoginState.complete);

        // usually its a good idea to save user object
        // but api is not returning user object
        // so just saving a boolean value for now
        await PrefsService.setBool(userPrefsKey, true);
      } else {
        _errorMessage = result.data['message'];
        _setState(LoginState.error);
      }
    } on DioError catch (e) {
      _errorMessage = e.message;
      _setState(LoginState.error);
    }
  }

  bool validateForm() {
    if (formKey.currentState!.validate()) {
      return true;
    }
    return false;
  }

  void _setState(LoginState state) {
    _state = state;
    notifyListeners();
  }
}

enum LoginState {
  initial,
  loading,
  complete,
  error,
}
