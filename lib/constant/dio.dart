import 'package:dio/dio.dart';

class MyDio {
  static const String baseUrl = 'https://api.whitewular.com';

  // parameters and other stuff can be passed in BaseOptions()
  static BaseOptions options = BaseOptions(
    baseUrl: baseUrl,
  );

  static Future<Dio> provideDio() async {
    final Dio dio = Dio(options);
    return dio;
  }
}
