import 'package:flutter/material.dart';
import 'package:white_wular/constant/strings.dart';
import 'package:white_wular/pages/auth_pages/login_page.dart';
import 'package:white_wular/pages/auth_pages/register_pages/register_page.dart';
import 'package:white_wular/utils/helpers.dart';
import 'package:white_wular/widgets/buttons/primary_button.dart';

class MainAuthPage extends StatelessWidget {
  const MainAuthPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(35.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            spacer(height: 60.0),
            // branding

            Text(
              'Welcome to',
              style: Theme.of(context).textTheme.headline6?.copyWith(
                    fontWeight: FontWeight.bold,
                  ),
            ),
            Hero(
              tag: 'appName',
              child: Text(
                'White Wular',
                style: Theme.of(context).textTheme.headline3?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).colorScheme.primary,
                    ),
              ),
            ),

            spacer(height: 12.0),
            const Text(kSlogan),

            // buttons
            const Spacer(),
            Hero(
              tag: 'registerBtn',
              child: PrimaryButton(
                text: 'Register',
                onPressed: () => goToPage(context, const RegisterPage()),
              ),
            ),
            Hero(
              tag: 'loginBtn',
              child: PrimaryButton(
                text: 'Login',
                onPressed: () => goToPage(context, const LoginPage()),
                isPrimary: false,
              ),
            ),
            // spacer(height: 40.0),
          ],
        ),
      ),
    );
  }
}
