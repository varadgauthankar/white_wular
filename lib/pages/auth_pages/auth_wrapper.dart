import 'package:flutter/material.dart';
import 'package:white_wular/pages/auth_pages/main_auth_page.dart';
import 'package:white_wular/pages/home_page.dart';

class AuthWrapper extends StatelessWidget {
  final bool isUserLoggedIn;
  const AuthWrapper({Key? key, required this.isUserLoggedIn}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return isUserLoggedIn ? const HomePage() : const MainAuthPage();
  }
}
