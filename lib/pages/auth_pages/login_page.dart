import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:white_wular/pages/home_page.dart';
import 'package:white_wular/provider/auth_providers/login_provider.dart';

import 'package:white_wular/utils/helpers.dart';
import 'package:white_wular/utils/validator.dart';
import 'package:white_wular/widgets/indicators/my_circular_progress.dart';

import 'package:white_wular/widgets/inputs/text_fields/password_text_field.dart';
import 'package:white_wular/widgets/inputs/text_fields/primary_text_field.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<LoginProvider>(
      builder: (context, provider, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: const EdgeInsets.all(35.0),
                child: Form(
                  key: provider.formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      spacer(height: 60.0),
                      Text(
                        'Login',
                        style: Theme.of(context).textTheme.headline3?.copyWith(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                      ),
                      Text(
                        'To continue',
                        style: Theme.of(context).textTheme.headline6?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),

                      spacer(height: 14.0),

                      // text fields

                      PrimaryTextField(
                        labelText: 'Email',
                        controller: provider.emailController,
                        validator: (input) => Validators.isValidEmail(input),
                      ),

                      spacer(height: 14.0),

                      PasswordTextField(
                        obscureText: true,
                        labelText: 'Password',
                        controller: provider.passwordController,
                        validator: (value) => Validators.isValidPassword(value),
                      ),

                      spacer(height: 10.0),
                      Text(
                        provider.errorMessage ?? '',
                        style: Theme.of(context).textTheme.caption?.copyWith(
                              color: Theme.of(context).colorScheme.error,
                            ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton.extended(
            heroTag: 'loginBtn',
            icon: provider.state == LoginState.loading
                ? _showCircularProgress()
                : const Icon(Icons.done),
            onPressed: () {
              if (provider.validateForm()) {
                provider.login().then((value) {
                  if (provider.state == LoginState.complete) {
                    replaceAllPages(context, const HomePage());
                  }
                });
              }
            },
            label: const Text('Login'),
          ),
        );
      },
    );
  }

  Row _showCircularProgress() {
    return Row(
      // just to align it properly
      children: [const MyCircularProgressIndicator(), spacer(width: 10)],
    );
  }
}
