import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:white_wular/pages/auth_pages/register_pages/register_password_page.dart';
import 'package:white_wular/provider/auth_providers/register_provider.dart';
import 'package:white_wular/utils/helpers.dart';
import 'package:white_wular/widgets/buttons/small_filled_Icon_button.dart';
import 'package:white_wular/widgets/inputs/drop_downs/drop_down_menu.dart';
import 'package:white_wular/widgets/inputs/text_fields/primary_text_field.dart';

class RegisterDetailsPage extends StatelessWidget {
  const RegisterDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<RegisterProvider>(
      builder: (context, provider, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: const EdgeInsets.all(35.0),
                child: Form(
                  key: provider.formKey2,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      spacer(height: 60.0),
                      Text(
                        'Enter Your',
                        style: Theme.of(context).textTheme.headline6?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      Text(
                        'Details',
                        style: Theme.of(context).textTheme.headline3?.copyWith(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                      ),

                      spacer(height: 14.0),

                      // text fields

                      PrimaryTextField(
                        controller: provider.birthDateController,
                        labelText: 'Birth Date',
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Enter your birth date';
                          }
                          return null;
                        },
                        readOnly: true,
                        onTap: () => provider.pickDate(context),
                      ),
                      spacer(height: 14.0),

                      MyDropDownMenu(
                        label: 'Gender',
                        items: const [
                          'Male',
                          'Female',
                          'Other',
                        ], // hard coded for now
                        onChanged: (value) => provider.setGender(value!),
                        value: provider.gender,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please choose your gender';
                          }
                          return null;
                        },
                      ),
                      spacer(height: 14.0),

                      MyDropDownMenu(
                        label: 'Profession',
                        items: const [
                          'Student',
                          'Businessman',
                          'Self Employed',
                        ], // hard coded for now
                        onChanged: (value) => provider.setProfession(value!),
                        value: provider.profession,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please choose your profession';
                          }
                          return null;
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton.extended(
            heroTag: 'registerBtn',
            onPressed: () {
              if (provider.validateForm2()) {
                goToPage(context, const RegisterPasswordPage());
              }
            },
            icon: const Icon(Icons.chevron_right),
            label: const Text('Next'),
          ),
        );
      },
    );
  }
}
