import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:white_wular/provider/auth_providers/register_provider.dart';
import 'package:white_wular/utils/helpers.dart';
import 'package:white_wular/utils/validator.dart';
import 'package:white_wular/widgets/buttons/small_filled_Icon_button.dart';
import 'package:white_wular/widgets/inputs/text_fields/primary_text_field.dart';

import 'register_details_page.dart';

class RegisterPage extends StatelessWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<RegisterProvider>(
      builder: (context, provider, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: const EdgeInsets.all(35.0),
                child: Form(
                  key: provider.formKey1,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      spacer(height: 60.0),

                      Hero(
                        tag: 'appName',
                        child: Text(
                          'Register',
                          style: Theme.of(context)
                              .textTheme
                              .headline3
                              ?.copyWith(
                                fontWeight: FontWeight.bold,
                                color: Theme.of(context).colorScheme.primary,
                              ),
                        ),
                      ),
                      Text(
                        'To Continue',
                        style: Theme.of(context).textTheme.headline6?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      spacer(height: 14.0),

                      // text fields

                      PrimaryTextField(
                        controller: provider.firstNameController,
                        labelText: 'First Name',
                        validator: (value) => Validators.isValidText(value),
                      ),
                      spacer(height: 10.0),
                      PrimaryTextField(
                        controller: provider.lastNameController,
                        labelText: 'Last Name',
                        validator: (value) => Validators.isValidText(value),
                      ),
                      spacer(height: 10.0),
                      PrimaryTextField(
                        controller: provider.emailController,
                        labelText: 'Email',
                        validator: (value) => Validators.isValidEmail(value),
                      ),
                      spacer(height: 10.0),
                      PrimaryTextField(
                        controller: provider.phoneNumberController,
                        labelText: 'Phone Number',
                        validator: (value) =>
                            Validators.isValidPhoneNumber(value),
                      ),
                      const Spacer(),
                    ],
                  ),
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton.extended(
            heroTag: 'registerBtn',
            onPressed: () {
              if (provider.validateForm1()) {
                goToPage(context, const RegisterDetailsPage());
              }
            },
            icon: const Icon(Icons.chevron_right),
            label: const Text('Next'),
          ),
        );
      },
    );
  }
}
