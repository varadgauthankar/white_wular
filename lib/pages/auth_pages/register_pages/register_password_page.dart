import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:white_wular/pages/auth_pages/register_pages/register_details_page.dart';
import 'package:white_wular/pages/home_page.dart';
import 'package:white_wular/provider/auth_providers/register_provider.dart';
import 'package:white_wular/utils/helpers.dart';
import 'package:white_wular/utils/validator.dart';
import 'package:white_wular/widgets/buttons/small_filled_Icon_button.dart';
import 'package:white_wular/widgets/indicators/my_circular_progress.dart';
import 'package:white_wular/widgets/inputs/text_fields/password_text_field.dart';

class RegisterPasswordPage extends StatelessWidget {
  const RegisterPasswordPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<RegisterProvider>(
      builder: (context, provider, child) {
        return Scaffold(
          body: SingleChildScrollView(
            child: SizedBox(
              height: MediaQuery.of(context).size.height,
              child: Padding(
                padding: const EdgeInsets.all(35.0),
                child: Form(
                  key: provider.formKey3,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      spacer(height: 60.0),
                      Text(
                        'Create a Strong',
                        style: Theme.of(context).textTheme.headline6?.copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                      ),
                      Text(
                        'Password',
                        style: Theme.of(context).textTheme.headline3?.copyWith(
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).colorScheme.primary,
                            ),
                      ),

                      spacer(height: 14.0),

                      // text fields

                      PasswordTextField(
                        obscureText: true,
                        labelText: 'Password',
                        controller: provider.passwordController,
                        validator: (value) => Validators.isValidPassword(value),
                      ),

                      spacer(height: 10.0),
                      PasswordTextField(
                        obscureText: true,
                        labelText: 'Confirm password',
                        controller: provider.confirmPasswordController,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please confirm your password';
                          }

                          if (provider.passwordController.text !=
                              provider.confirmPasswordController.text) {
                            return 'Password does not match';
                          }

                          return null;
                        },
                      ),
                      spacer(height: 10.0),
                      Text(
                        provider.errorMessage ?? '',
                        style: Theme.of(context).textTheme.caption?.copyWith(
                              color: Theme.of(context).colorScheme.error,
                            ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton.extended(
            heroTag: 'registerBtn',
            icon: provider.state == RegisterState.loading
                ? _showCircularProgress()
                : const Icon(Icons.done),
            onPressed: () async {
              if (provider.validateForm3()) {
                provider.register().then((value) {
                  if (provider.state == RegisterState.complete) {
                    replaceAllPages(context, const HomePage());
                  }
                });
              }
            },
            label: const Text('Register'),
          ),
        );
      },
    );
  }

  Row _showCircularProgress() {
    return Row(
      // just to align it properly
      children: [const MyCircularProgressIndicator(), spacer(width: 10)],
    );
  }
}
