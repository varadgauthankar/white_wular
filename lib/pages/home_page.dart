import 'package:flutter/material.dart';
import 'package:white_wular/constant/prefs_keys.dart';
import 'package:white_wular/constant/strings.dart';
import 'package:white_wular/pages/auth_pages/main_auth_page.dart';
import 'package:white_wular/services/prefs.dart';
import 'package:white_wular/utils/helpers.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(kAppName),
      ),
      body: Center(
        child: TextButton(
          child: const Text('Log out'),
          onPressed: () async {
            // need to be done using provider and maybe api too
            // but for now, we will just clear the shared prefs
            PrefsService.remove(userPrefsKey).then(
              (value) {
                replaceAllPages(context, const MainAuthPage());
              },
            );
          },
        ),
      ),
    );
  }
}
