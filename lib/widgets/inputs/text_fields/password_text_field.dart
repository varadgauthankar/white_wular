import 'package:flutter/material.dart';

class PasswordTextField extends StatelessWidget {
  final String labelText;
  final VoidCallback? onTap;
  final bool readOnly;
  final TextEditingController controller;
  final String? Function(String?)? validator;
  final bool obscureText;

  const PasswordTextField({
    Key? key,
    required this.labelText,
    this.onTap,
    this.readOnly = false,
    required this.controller,
    this.validator,
    this.obscureText = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obscureText,
      controller: controller,
      validator: validator,
      onTap: onTap,
      readOnly: readOnly,
      decoration: InputDecoration(
        labelText: labelText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
        ),
      ),
    );
  }
}
