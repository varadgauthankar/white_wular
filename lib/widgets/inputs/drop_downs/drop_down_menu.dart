import 'package:flutter/material.dart';

class MyDropDownMenu extends StatelessWidget {
  final List<String> items;
  final String? value;
  final Function(String?)? onChanged;
  final String? Function(dynamic)? validator;
  final String label;

  const MyDropDownMenu({
    Key? key,
    required this.items,
    this.value,
    this.onChanged,
    this.validator,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
      validator: validator,
      decoration: InputDecoration(
        label: Text(label),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(8)),
      ),
      value: value,
      items: items
          .map((e) => DropdownMenuItem(
                value: e,
                child: Text(e),
              ))
          .toList(),
      onChanged: onChanged,
    );
  }
}
